//未重构前
var index = require('./index');
var index_1 = require('./index-1');
var index_2 = require('./index-2');
var fs = require("fs");

var jsonPlays = fs.readFileSync("plays.json").toString()
var jsonInvoice = fs.readFileSync("invoice.json").toString()

//未优化前
//var result = index.statement(JSON.parse(jsonInvoice)[0], JSON.parse(jsonPlays))

//优化1，函数拆分
// var result = index_1.statement(JSON.parse(jsonInvoice)[0], JSON.parse(jsonPlays))

//优化2，先消除局部变量
var result = index_2.statement(JSON.parse(jsonInvoice)[0], JSON.parse(jsonPlays))


//输出
console.log(result)